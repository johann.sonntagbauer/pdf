package at.myflexbox.pdf.render;

import at.myflexbox.pdf.model.DataEntry;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPageContentStream;

public class RenderContext {
    private final DataEntry data;
    private final float documentHeight;
    private final float documentWidth;
    private final PDDocument document;
    private final PDPageContentStream contentStream;

    public RenderContext(DataEntry data, float documentWidth, float documentHeight, PDDocument document, PDPageContentStream contentStream) {
        this.data = data;
        this.documentHeight = documentHeight;
        this.documentWidth = documentWidth;
        this.document = document;
        this.contentStream = contentStream;
    }

    public DataEntry getData() {
        return data;
    }

    public float getDocumentHeight() {
        return documentHeight;
    }

    public float getDocumentWidth() {
        return documentWidth;
    }

    public PDDocument getDocument() {
        return document;
    }

    public PDPageContentStream getContentStream() {
        return contentStream;
    }
}
