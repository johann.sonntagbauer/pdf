package at.myflexbox.pdf.render;

import at.myflexbox.pdf.expression.ExpressionEvaluator;
import at.myflexbox.pdf.model.template.QRCode;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import java.awt.image.BufferedImage;
import java.io.IOException;

import static at.myflexbox.pdf.render.PositionCalculations.*;

public class QRRenderer implements ObjectRenderer<QRCode> {

    public void render(QRCode qrCode, RenderContext context) throws WriterException, IOException {
        float x = mmToPoints(qrCode.getX());
        float y = mmToPoints(qrCode.getY());
        float height = mmToPoints(qrCode.getHeight());
        float width = mmToPoints(qrCode.getWidth());

        QRCodeWriter barcodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix =
                barcodeWriter.encode(ExpressionEvaluator.evaluate(qrCode.getValue(), context.getData()), BarcodeFormat.QR_CODE, Math.round(scaleToResolution(width, 600)), Math.round(scaleToResolution(height, 600)));
        BufferedImage bufferedImage = MatrixToImageWriter.toBufferedImage(bitMatrix);
        PDImageXObject image = LosslessFactory.createFromImage(context.getDocument(), bufferedImage);

        context.getContentStream().drawImage(image, x, adaptYCoordinates(context.getDocumentHeight(), y, height), width, height);
    }
}
