package at.myflexbox.pdf.render;

import at.myflexbox.pdf.model.DataEntry;
import at.myflexbox.pdf.model.template.Template;

import java.io.File;
import java.util.Iterator;
import java.util.concurrent.*;
import java.util.logging.Logger;

/**
 * Render a PDF for each {@link DataEntry} with the given template.
 * The PDF files will be written to the output directory.
 */
public class PDFRenderer {
    private final Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    public void render(Template template, Iterator<DataEntry> data, File output) throws Exception {

        // configure ThreadpoolExecutor to only accept a given amount of work and blocking afterwards
        // to ensure not all work items immediately turn into scheduled tasks and avoid memory pressure.
        ThreadPoolExecutor executorService = new ThreadPoolExecutor(2, Runtime.getRuntime().availableProcessors(),
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(Runtime.getRuntime().availableProcessors() * 2), new ThreadPoolExecutor.CallerRunsPolicy());

        ScheduledExecutorService monitor = Executors.newScheduledThreadPool(1);
        monitor.scheduleAtFixedRate(
                () -> logger.info("Progress completed " + executorService.getCompletedTaskCount() + " of " + executorService.getTaskCount()),
                10, 10, TimeUnit.SECONDS);

        int counter = 0;
        while (data.hasNext()) {
            executorService.submit(new RenderTask(template, data.next(), output, counter + ".pdf"));
            counter++;
        }

        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.HOURS);
        monitor.shutdown();
        logger.info("Generated " + counter + " PDF files in " + output.getAbsolutePath());
    }
}
