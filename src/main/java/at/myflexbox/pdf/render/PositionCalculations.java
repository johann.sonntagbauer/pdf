package at.myflexbox.pdf.render;

public class PositionCalculations {

    public static final int DEFAULT_DPI = 72;

    /**
     * scale value from default DPI to desired dpi resolution
     */
    public static float scaleToResolution(float points, float dpi) {
        return points / DEFAULT_DPI * dpi;
    }

    /**
     * mm to point translation based on PDRectangle code
     */
    public static float mmToPoints(int mm) {
        return mm * (1 / (10 * 2.54f) * DEFAULT_DPI);
    }

    /**
     * needed because the PDF coordinate system originates in the bottom left corner and the
     * template coordinate system originates in the top left corner. So the Y coordinates have to be flipped.
     */
    public static float adaptYCoordinates(float documentHeight, float y, float height) {
        return documentHeight - y - height;
    }
}
