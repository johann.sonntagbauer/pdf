package at.myflexbox.pdf.render;

import at.myflexbox.pdf.model.template.QRCode;
import at.myflexbox.pdf.model.template.TemplateObject;
import at.myflexbox.pdf.model.template.Text;

import java.util.logging.Logger;

public class TemplateObjectRenderer implements ObjectRenderer<TemplateObject> {

    private final Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    private final TextRenderer textRenderer;
    private final QRRenderer qrRenderer;

    public TemplateObjectRenderer() {
        textRenderer = new TextRenderer();
        qrRenderer = new QRRenderer();
    }

    @Override
    public void render(TemplateObject object, RenderContext context) throws Exception {
        if (object instanceof Text) {
            textRenderer.render((Text) object, context);
        } else if (object instanceof QRCode) {
            qrRenderer.render((QRCode) object, context);
        } else {
            logger.severe("Failed to render object " + object + " because type is not supported");
        }
    }
}
