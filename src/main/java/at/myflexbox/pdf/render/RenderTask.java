package at.myflexbox.pdf.render;

import at.myflexbox.pdf.model.DataEntry;
import at.myflexbox.pdf.model.template.Template;
import at.myflexbox.pdf.model.template.TemplateObject;
import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;

import java.io.File;
import java.util.concurrent.Callable;

import static at.myflexbox.pdf.render.PositionCalculations.mmToPoints;

public class RenderTask implements Callable<Void> {

    private final Template template;
    private final DataEntry data;
    private final File outputDirectory;
    private final String outputFileName;

    public RenderTask(Template template, DataEntry data, File outputDirectory, String outputFileName) {
        this.template = template;
        this.data = data;
        this.outputDirectory = outputDirectory;
        this.outputFileName = outputFileName;
    }

    @Override
    public Void call() throws Exception {
        try (PDDocument doc = new PDDocument(MemoryUsageSetting.setupMainMemoryOnly())) {
            float width = mmToPoints(template.getPage().getWidth());
            float height = mmToPoints(template.getPage().getHeight());
            TemplateObjectRenderer templateObjectRenderer = new TemplateObjectRenderer();

            PDPage page = new PDPage(new PDRectangle(width, height));
            doc.addPage(page);

            try (PDPageContentStream contentStream = new PDPageContentStream(doc, page)) {
                RenderContext renderContext = new RenderContext(data, width, height, doc, contentStream);
                for (TemplateObject object : template.getObjects()) {
                    templateObjectRenderer.render(object, renderContext);
                }
            }
            doc.save(new File(outputDirectory, outputFileName));
        }
        return null;
    }
}
