package at.myflexbox.pdf.render;

import at.myflexbox.pdf.model.template.TemplateObject;

/**
 *
 * @param <T>
 */
public interface ObjectRenderer<T extends TemplateObject> {

    void render(T object, RenderContext context) throws Exception;
}
