package at.myflexbox.pdf.render;

import at.myflexbox.pdf.expression.ExpressionEvaluator;
import at.myflexbox.pdf.model.template.Text;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import java.io.IOException;

import static at.myflexbox.pdf.render.PositionCalculations.adaptYCoordinates;
import static at.myflexbox.pdf.render.PositionCalculations.mmToPoints;

public class TextRenderer implements ObjectRenderer<Text> {
    public void render(Text text, RenderContext context) throws IOException {
        float x = mmToPoints(text.getX());
        float y = mmToPoints(text.getY());

        PDPageContentStream contentStream = context.getContentStream();

        contentStream.beginText();
        contentStream.setFont(PDType1Font.COURIER, text.getFontSize());
        contentStream.newLineAtOffset(x, adaptYCoordinates(context.getDocumentHeight(), y, text.getFontSize()));
        contentStream.showText(ExpressionEvaluator.evaluate(text.getValue(), context.getData()));
        contentStream.endText();
    }
}
