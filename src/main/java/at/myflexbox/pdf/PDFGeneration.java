package at.myflexbox.pdf;


import at.myflexbox.pdf.command.PDFCommand;
import at.myflexbox.pdf.io.DataReader;
import at.myflexbox.pdf.io.TemplateReader;
import at.myflexbox.pdf.render.PDFRenderer;
import com.fasterxml.jackson.databind.ObjectMapper;
import picocli.CommandLine;

public class PDFGeneration {
    public static void main(String[] args) {

        ObjectMapper objectMapper = new ObjectMapper();
        TemplateReader templateReader = new TemplateReader(objectMapper);
        DataReader dataReader = new DataReader(objectMapper);
        PDFRenderer renderer = new PDFRenderer();
        PDFCommand pdfCommand = new PDFCommand(templateReader, dataReader, renderer);

        int exitCode = new CommandLine(pdfCommand).execute(args);
        System.exit(exitCode);
    }
}
