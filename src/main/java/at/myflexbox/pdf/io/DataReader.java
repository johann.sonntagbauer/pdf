package at.myflexbox.pdf.io;

import at.myflexbox.pdf.model.DataEntry;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Read the data from json file
 * <p>
 * Format of the file:
 * <pre>{@code
 * {
 *      "data": [
 *          DataEntry
 *      ]
 * }
 * }</pre>
 * A {@link DataEntry} can contain an arbitrary set of properties with only string values.
 */
public class DataReader {

    private final ObjectMapper objectMapper;

    public DataReader(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    private void expect(JsonToken token, JsonToken expected) {
        if (!expected.equals(token)) {
            throw new RuntimeException("failed to parse data because of token mismatch. Expected " + expected + " but read " + token);
        }
    }

    public Iterator<DataEntry> readData(Reader reader) throws IOException {
        JsonParser parser = objectMapper.createParser(reader);

        expect(parser.nextToken(), JsonToken.START_OBJECT);
        expect(parser.nextToken(), JsonToken.FIELD_NAME);
        // data field
        expect(parser.nextToken(), JsonToken.START_ARRAY);

        return new Iterator<>() {

            private DataEntry currentEntry = null;

            @Override
            public boolean hasNext() {
                // parse object entry in data array
                try {
                    if (parser.nextToken() != JsonToken.START_OBJECT) {
                        // no more objects in data array - close iterator
                        // and parser resources
                        currentEntry = null;
                        parser.close();
                        return false;
                    } else {
                        // read current data array entry
                        Map<String, String> entries = new HashMap<>();
                        while (parser.nextToken() != JsonToken.END_OBJECT) {
                            entries.put(parser.currentName(), parser.nextTextValue());
                        }
                        currentEntry = new DataEntry(entries);
                        return true;
                    }
                } catch (IOException e) {
                    throw new RuntimeException("failed to parse data json", e);
                }
            }

            @Override
            public DataEntry next() {
                return currentEntry;
            }
        };
    }
}
