package at.myflexbox.pdf.io;

import at.myflexbox.pdf.model.template.Template;
import at.myflexbox.pdf.model.validation.ValidationException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;

import java.io.IOException;
import java.io.Reader;
import java.util.Set;

/**
 * Read the {@link Template} from json file
 */
public class TemplateReader {

    private final ObjectReader objectReader;

    public TemplateReader(ObjectMapper objectMapper) {
        this.objectReader = objectMapper.readerFor(Template.class);
    }

    private void validateAndThrow(Template template) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<Template>> violations = validator.validate(template);

        if (violations.size() > 0) {
            throw new ValidationException(template, violations);
        }
    }

    public Template readTemplate(Reader reader) throws IOException {
        Template template;
        try (JsonParser parser = objectReader.createParser(reader)) {
            template = parser.readValueAs(Template.class);
        }
        validateAndThrow(template);

        return template;
    }

}
