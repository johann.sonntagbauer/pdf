package at.myflexbox.pdf.expression;

import at.myflexbox.pdf.model.DataEntry;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Evaluate expressions {&lt;property&gt;} and lookup the referenced properties in {@link DataEntry}
 */
public class ExpressionEvaluator {

    private static final Pattern PATTERN = Pattern.compile("\\{(\\w+)}");

    public static String evaluate(String expression, DataEntry data) {
        if (expression == null) {
            return null;
        }

        Matcher matcher = PATTERN.matcher(expression);
        return matcher.replaceAll((matchResult -> {
            String placeholder = matcher.group(1);
            if (!data.containsKey(placeholder)) {
                throw new RuntimeException("Parameter " + placeholder + " not found in data");
            }
            return data.get(placeholder);
        }));
    }
}
