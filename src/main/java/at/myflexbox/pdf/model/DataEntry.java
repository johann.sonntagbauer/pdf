package at.myflexbox.pdf.model;

import java.util.Map;
import java.util.Objects;

/**
 * Represents one entry in data json file
 */
public class DataEntry {
    private final Map<String, String> entry;

    public DataEntry(Map<String, String> entry) {
        this.entry = entry;
    }

    public int size() {
        return entry.size();
    }

    public boolean containsKey(Object key) {
        return entry.containsKey(key);
    }

    public String get(Object key) {
        return entry.get(key);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataEntry dataEntry = (DataEntry) o;
        return entry.equals(dataEntry.entry);
    }

    @Override
    public int hashCode() {
        return Objects.hash(entry);
    }
}
