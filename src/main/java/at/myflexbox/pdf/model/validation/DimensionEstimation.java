package at.myflexbox.pdf.model.validation;

import at.myflexbox.pdf.model.template.QRCode;
import at.myflexbox.pdf.model.template.TemplateObject;
import at.myflexbox.pdf.model.template.Text;

import java.awt.*;
import java.util.Optional;

public class DimensionEstimation {

    /**
     * Try to estimate the rendered dimensions to proper validate placement constraints.
     */
    public static Optional<Rectangle> forObject(TemplateObject object) {
        if (object instanceof QRCode) {
            QRCode qr = (QRCode) object;
            return Optional.of(new Rectangle(qr.getX(), qr.getY(), qr.getWidth(), qr.getHeight()));
        } else if (object instanceof Text) {
            Text text = (Text) object;
            // TODO: can we estimate the dimensions of the rendered text?
            return Optional.of(new Rectangle(text.getX(), text.getY(), 1, 1));
        } else {
            return Optional.empty();
        }
    }
}
