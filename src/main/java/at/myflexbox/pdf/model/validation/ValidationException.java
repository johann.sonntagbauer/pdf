package at.myflexbox.pdf.model.validation;

import jakarta.validation.ConstraintViolation;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * Represents all constraint violations of a validation in a more user friendly representation.
 */
public class ValidationException extends RuntimeException {

    public <T> ValidationException(T data, Set<ConstraintViolation<T>> violations) {
        super(data.getClass().getSimpleName() + " contains following errors: \n" +
                violations.stream().map((error) -> "field: '" + error.getPropertyPath() + "' " + error.getMessage())
                        .collect(Collectors.joining("\n"))
                + "\n" +
                "parsed representation: " + data);
    }
}
