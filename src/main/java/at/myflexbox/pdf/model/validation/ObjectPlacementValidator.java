package at.myflexbox.pdf.model.validation;

import at.myflexbox.pdf.model.template.Template;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.awt.*;
import java.util.concurrent.atomic.AtomicBoolean;
/**
 * Validate if a renderable template object can be displayed on the page.
 * If the template object is placed outside the page, a constraint violation will be triggered.
 */
public class ObjectPlacementValidator implements ConstraintValidator<ObjectPlacement, Template> {

    @Override
    public boolean isValid(Template template, ConstraintValidatorContext context) {
        Rectangle dim = new Rectangle(template.getPage().getWidth(), template.getPage().getHeight());
        AtomicBoolean isValid = new AtomicBoolean(true);

        template.getObjects()
                .forEach(object -> DimensionEstimation.forObject(object)
                        .ifPresent((rectangle -> {
                            if (!dim.contains(rectangle)) {
                                isValid.set(false);
                                context.disableDefaultConstraintViolation();
                                context
                                        .buildConstraintViolationWithTemplate(object + " exceeds " + template.getPage() + " bounds ")
                                        .addPropertyNode("objects")
                                        .addConstraintViolation();
                            }
                        }))
                );

        return isValid.get();
    }
}
