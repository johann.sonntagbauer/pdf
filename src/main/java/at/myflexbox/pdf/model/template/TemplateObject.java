package at.myflexbox.pdf.model.template;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;

import java.util.Objects;
import java.util.StringJoiner;

/**
 * Common base Type to combine shared render aspects.
 * Mainly needed to proper map discriminated unions with jackson.
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type")
@JsonSubTypes({
        @Type(value = QRCode.class, name = QRCode.DISCRIMINATOR),
        @Type(value = Text.class, name = Text.DISCRIMINATOR)
})
public abstract class TemplateObject {

    @PositiveOrZero
    private final int x;
    @PositiveOrZero
    private final int y;
    private final String type;
    @NotNull
    private final String value;

    public TemplateObject(int x, int y, String type, String value) {
        this.x = x;
        this.y = y;
        this.type = type;
        this.value = value;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public String getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TemplateObject that = (TemplateObject) o;
        return x == that.x && y == that.y && type.equals(that.type) && value.equals(that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, type, value);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", TemplateObject.class.getSimpleName() + "[", "]")
                .add("x=" + x)
                .add("y=" + y)
                .add("type='" + type + "'")
                .add("value='" + value + "'")
                .toString();
    }
}
