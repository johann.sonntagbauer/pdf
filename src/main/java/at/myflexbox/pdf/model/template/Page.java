package at.myflexbox.pdf.model.template;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.PositiveOrZero;

import java.util.Objects;
import java.util.StringJoiner;

/**
 * Page meta data for template
 */
public class Page {

    @PositiveOrZero
    private final int width;
    @PositiveOrZero
    private final int height;

    @JsonCreator
    public Page(
            @JsonProperty(value = "width", required = true) int width,
            @JsonProperty(value = "height", required = true) int height
    ) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Page page = (Page) o;
        return width == page.width && height == page.height;
    }

    @Override
    public int hashCode() {
        return Objects.hash(width, height);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Page.class.getSimpleName() + "[", "]")
                .add("width=" + width)
                .add("height=" + height)
                .toString();
    }
}
