package at.myflexbox.pdf.model.template;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import jakarta.validation.constraints.PositiveOrZero;

import java.util.Objects;
import java.util.StringJoiner;

import static at.myflexbox.pdf.model.template.QRCode.DISCRIMINATOR;

/**
 * Render description for QR Codes
 */
@JsonTypeName(DISCRIMINATOR)
public class QRCode extends TemplateObject {

    public static final String DISCRIMINATOR = "QR";

    @PositiveOrZero
    private final int width;
    @PositiveOrZero
    private final int height;

    @JsonCreator
    public QRCode(
            @JsonProperty(value = "x", required = true) int x,
            @JsonProperty(value = "y", required = true) int y,
            @JsonProperty(value = "value", required = true) String value,
            @JsonProperty(value = "width", required = true) int width,
            @JsonProperty(value = "height", required = true) int height
    ) {
        super(x, y, DISCRIMINATOR, value);
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        QRCode qrCode = (QRCode) o;
        return width == qrCode.width && height == qrCode.height;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), width, height);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", QRCode.class.getSimpleName() + "[", "]")
                .add("x=" + getX())
                .add("y=" + getY())
                .add("type='" + getType() + "'")
                .add("value='" + getValue() + "'")
                .add("width=" + width)
                .add("height=" + height)
                .toString();
    }
}
