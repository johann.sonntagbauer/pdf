package at.myflexbox.pdf.model.template;

import at.myflexbox.pdf.model.validation.ObjectPlacement;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Size;

import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * Represents whole template
 *
 * A template consists of metadata of the {@link Page} and a list of renderable objects called {@link TemplateObject}.
 *
 * {
 *   "page": {
 *     "width": 300,
 *     "height": 200
 *   },
 *   "objects": [
 *     {
 *       "x": 10,
 *       "y": 10,
 *       "fontsize": 12,
 *       "type": "text",
 *       "value": "{parcelnumber}"
 *     },
 */
@ObjectPlacement
public class Template {

    @Valid
    private final Page page;
    @Valid
    @Size(min = 1)
    private final List<TemplateObject> objects;

    @JsonCreator
    public Template(
            @JsonProperty(value = "page", required = true) Page page,
            @JsonProperty(value = "objects", required = true) List<TemplateObject> objects
    ) {
        this.page = page;
        this.objects = objects;
    }

    public Page getPage() {
        return page;
    }

    public List<TemplateObject> getObjects() {
        return objects;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Template template = (Template) o;
        return page.equals(template.page) && objects.equals(template.objects);
    }

    @Override
    public int hashCode() {
        return Objects.hash(page, objects);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Template.class.getSimpleName() + "[", "]")
                .add("page=" + page)
                .add("objects=" + objects)
                .toString();
    }
}
