package at.myflexbox.pdf.model.template;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import jakarta.validation.constraints.PositiveOrZero;

import java.util.Objects;
import java.util.StringJoiner;

import static at.myflexbox.pdf.model.template.Text.DISCRIMINATOR;

/**
 * Render description for Text
 */
@JsonTypeName(DISCRIMINATOR)
public class Text extends TemplateObject {
    public static final String DISCRIMINATOR = "text";

    @PositiveOrZero
    private final float fontSize;

    @JsonCreator
    public Text(
            @JsonProperty(value = "x", required = true) int x,
            @JsonProperty(value = "y", required = true) int y,
            @JsonProperty(value = "value", required = true) String value,
            @JsonProperty(value = "fontsize", required = true) float fontSize
    ) {
        super(x, y, DISCRIMINATOR, value);
        this.fontSize = fontSize;
    }

    public float getFontSize() {
        return fontSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Text text = (Text) o;
        return Float.compare(text.fontSize, fontSize) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), fontSize);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Text.class.getSimpleName() + "[", "]")
                .add("x=" + getX())
                .add("y=" + getY())
                .add("type='" + getType() + "'")
                .add("value='" + getValue() + "'")
                .add("fontSize=" + fontSize)
                .toString();
    }
}
