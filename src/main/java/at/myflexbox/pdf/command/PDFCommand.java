package at.myflexbox.pdf.command;

import at.myflexbox.pdf.io.DataReader;
import at.myflexbox.pdf.io.TemplateReader;
import at.myflexbox.pdf.model.DataEntry;
import at.myflexbox.pdf.model.template.Template;
import at.myflexbox.pdf.render.PDFRenderer;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.nio.file.Files;
import java.util.Iterator;
import java.util.concurrent.Callable;
import java.util.logging.Logger;

/**
 * Command Line interface for rendering
 * a template with data provided in json format as PDF in an output directory
 */
@Command(name = "render", mixinStandardHelpOptions = true)
public class PDFCommand implements Callable<Integer> {

    Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    @Option(names = {"-t", "--template"}, description = "Template file", required = true)
    private File template;

    @Option(names = {"-d", "--data"}, description = "Data file", required = true)
    private File data;

    @Option(names = {"-o", "--output"}, description = "Output directory", required = true)
    private File output;

    private final TemplateReader templateReader;
    private final DataReader dataReader;
    private final PDFRenderer renderer;

    public PDFCommand(TemplateReader templateReader, DataReader dataReader, PDFRenderer renderer) {
        this.templateReader = templateReader;
        this.dataReader = dataReader;
        this.renderer = renderer;
    }

    private void assertFileExists(File file) throws FileNotFoundException {
        if (!file.exists()) {
            throw new FileNotFoundException("File " + file.getAbsolutePath() + " must exist.");
        }
        if (!file.isFile()) {
            throw new FileNotFoundException("File " + file.getAbsolutePath() + " must be a regular file.");
        }
    }

    @Override
    public Integer call() throws Exception {
        logger.info("read template from " + template.getAbsolutePath());
        assertFileExists(template);
        Template parsedTemplate = templateReader.readTemplate(new FileReader(template));
        logger.fine(() -> "parsed template " + parsedTemplate);

        logger.info("prepare data stream from " + data.getAbsolutePath());
        assertFileExists(data);
        Iterator<DataEntry> parsedData = dataReader.readData(new FileReader(data));

        logger.info("write PDF to " + output.getAbsolutePath());
        if (output.exists() && !output.isDirectory()) {
            throw new FileNotFoundException("Output " + output.getAbsolutePath() + " must reference an directory");
        } else {
            Files.createDirectories(output.toPath());
        }
        renderer.render(parsedTemplate, parsedData, output);
        logger.info("DONE");

        return 0;
    }
}
