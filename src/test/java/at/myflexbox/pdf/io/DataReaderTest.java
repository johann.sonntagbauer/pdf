package at.myflexbox.pdf.io;

import at.myflexbox.pdf.model.DataEntry;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringReader;
import java.util.Iterator;
import java.util.Map;

public class DataReaderTest {

    private DataReader getDataReader() {
        return new DataReader(new ObjectMapper());
    }

    @Test
    public void syntaxErrorTest() {
        Assertions.assertThrows(RuntimeException.class, () -> getDataReader().readData(new StringReader("")));
        Assertions.assertThrows(RuntimeException.class, () -> getDataReader().readData(new StringReader("{}")));
        Assertions.assertThrows(RuntimeException.class, () -> getDataReader().readData(new StringReader("{\"data\": {}}")));
    }

    @Test
    public void emptyTest() throws IOException {
        Iterator<DataEntry> mapIterator = getDataReader().readData(new StringReader("{\"data\": []}"));
        Assertions.assertFalse(mapIterator.hasNext());
    }

    @Test
    public void entriesTest() throws IOException {
        Iterator<DataEntry> mapIterator = getDataReader().readData(new StringReader(
                // language=JSON
                "{\n" +
                        "  \"data\": [\n" +
                        "    {\n" +
                        "      \"name\": \"karl\"\n" +
                        "    },\n" +
                        "    {\n" +
                        "      \"name\": \"susi\"\n" +
                        "    }\n" +
                        "]\n" +
                        "}"
        ));
        Assertions.assertTrue(mapIterator.hasNext());
        Assertions.assertEquals(new DataEntry(Map.of("name", "karl")), mapIterator.next());
        Assertions.assertTrue(mapIterator.hasNext());
        Assertions.assertEquals(new DataEntry(Map.of("name", "susi")), mapIterator.next());
        Assertions.assertFalse(mapIterator.hasNext());
    }

}
