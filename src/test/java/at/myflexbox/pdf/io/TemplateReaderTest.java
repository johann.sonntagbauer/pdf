package at.myflexbox.pdf.io;

import at.myflexbox.pdf.model.template.Page;
import at.myflexbox.pdf.model.template.QRCode;
import at.myflexbox.pdf.model.template.Template;
import at.myflexbox.pdf.model.template.Text;
import at.myflexbox.pdf.model.validation.ValidationException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.stream.Stream;

class TemplateReaderTest {

    private TemplateReader getTemplateReader() {
        return new TemplateReader(new ObjectMapper());
    }

    @Test
    void pageWithTextObject() throws IOException {
        Template template = getTemplateReader().readTemplate(new StringReader(
                // language=JSON
                "{\n" +
                        "  \"page\": {\n" +
                        "    \"width\": 12,\n" +
                        "    \"height\": 12\n" +
                        "  },\n" +
                        "  \"objects\": [\n" +
                        "    {\n" +
                        "      \"type\": \"text\",\n" +
                        "      \"x\": 2,\n" +
                        "      \"y\": 2,\n" +
                        "      \"value\": \"\",\n" +
                        "      \"fontsize\": 12\n" +
                        "    }\n" +
                        "  ]\n" +
                        "}"
        ));

        Template reference = new Template(
                new Page(12, 12),
                List.of(new Text(2, 2, "", 12))
        );

        Assertions.assertEquals(reference, template);
    }

    @Test
    void pageWithQRObject() throws IOException {
        Template template = getTemplateReader().readTemplate(new StringReader(
                // language=JSON
                "{\n" +
                        "  \"page\": {\n" +
                        "    \"width\": 12,\n" +
                        "    \"height\": 12\n" +
                        "  },\n" +
                        "  \"objects\": [\n" +
                        "    {\n" +
                        "      \"type\": \"QR\",\n" +
                        "      \"x\": 2,\n" +
                        "      \"y\": 2,\n" +
                        "      \"value\": \"\",\n" +
                        "      \"width\": 3,\n" +
                        "      \"height\": 3\n" +
                        "    }\n" +
                        "  ]\n" +
                        "}"
        ));

        Template reference = new Template(
                new Page(12, 12),
                List.of(new QRCode(2, 2, "", 3, 3))
        );

        Assertions.assertEquals(reference, template);
    }

    private static Stream<Arguments> provideInvalidJson() {
        return Stream.of(
                Arguments.of("{}"),
                Arguments.of("{\"page\": {}}"),
                Arguments.of("{\"page\": {\"width\": 12}}"),
                Arguments.of("{\"page\": {\"height\": 12}}"),
                Arguments.of("{\"page\": {\"width\": 12, \"height\": 12}}"),
                Arguments.of("{\"page\": {\"width\": 12, \"height\": 12}, \"objects\": [{\"type\": \"TEXT\", \"x\": 2, \"y\": 2, \"value\": \"\", \"fontsize\": 12}]}"),
                Arguments.of("{\"page\": {\"width\": 12, \"height\": 12}, \"objects\": [{\"type\": \"asdf\", \"x\": 2, \"y\": 2, \"value\": \"\", \"fontsize\": 12}]}"),
                Arguments.of("{\"page\": {\"width\": 12, \"height\": 12}, \"objects\": [{\"type\": \"text\", \"y\": 2, \"value\": \"\", \"fontsize\": 12}]}"),
                Arguments.of("{\"page\": {\"width\": 12, \"height\": 12}, \"objects\": [{\"type\": \"text\", \"x\": 2, \"value\": \"\", \"fontsize\": 12}]}"),
                Arguments.of("{\"page\": {\"width\": 12, \"height\": 12}, \"objects\": [{\"type\": \"text\", \"x\": 2, \"y\": 2, \"fontsize\": 12}]}"),
                Arguments.of("{\"page\": {\"width\": 12, \"height\": 12}, \"objects\": [{\"type\": \"text\", \"x\": 2, \"y\": 2, \"value\": \"\"}]}"),
                Arguments.of("{\"page\": {\"width\": 12, \"height\": 12}, \"objects\": [{\"type\": \"QR\", \"y\": 2, \"value\": \"\", \"width\": 3, \"height\": 3}]}"),
                Arguments.of("{\"page\": {\"width\": 12, \"height\": 12}, \"objects\": [{\"type\": \"QR\", \"x\": 2, \"value\": \"\", \"width\": 3, \"height\": 3}]}"),
                Arguments.of("{\"page\": {\"width\": 12, \"height\": 12}, \"objects\": [{\"type\": \"QR\", \"x\": 2, \"y\": 2, \"width\": 3, \"height\": 3}]}"),
                Arguments.of("{\"page\": {\"width\": 12, \"height\": 12}, \"objects\": [{\"type\": \"QR\", \"x\": 2, \"y\": 2, \"value\": \"\", \"height\": 3}]}"),
                Arguments.of("{\"page\": {\"width\": 12, \"height\": 12}, \"objects\": [{\"type\": \"QR\", \"x\": 2, \"y\": 2, \"value\": \"\", \"width\": 3 }]}")
        );
    }

    private static Stream<Arguments> provideOutOfRangeProperties() {
        return Stream.of(
                Arguments.of("{\"page\": {\"width\": -12, \"height\": 12}, \"objects\": [{\"type\": \"text\", \"x\": 2, \"y\": 2, \"value\": \"\", \"fontsize\": 12}]}"),
                Arguments.of("{\"page\": {\"width\": 12, \"height\": -12}, \"objects\": [{\"type\": \"text\", \"x\": 2, \"y\": 2, \"value\": \"\", \"fontsize\": 12}]}"),
                Arguments.of("{\"page\": {\"width\": 12, \"height\": 12}, \"objects\": [{\"type\": \"text\", \"x\": -2, \"y\": 2, \"value\": \"\", \"fontsize\": 12}]}"),
                Arguments.of("{\"page\": {\"width\": 12, \"height\": 12}, \"objects\": [{\"type\": \"text\", \"x\": 2, \"y\": -2, \"value\": \"\", \"fontsize\": 12}]}"),
                Arguments.of("{\"page\": {\"width\": 12, \"height\": 12}, \"objects\": [{\"type\": \"text\", \"x\": 2, \"y\": 2, \"value\": \"\", \"fontsize\": -12}]}"),
                Arguments.of("{\"page\": {\"width\": 12, \"height\": 12}, \"objects\": [{\"type\": \"text\", \"x\": 2, \"y\": 2, \"value\": null, \"fontsize\": 12}]}"),
                Arguments.of("{\"page\": {\"width\": 12, \"height\": 12}, \"objects\": []}"),
                Arguments.of("{\"page\": {\"width\": 12, \"height\": 12}, \"objects\": [{\"type\": \"QR\", \"x\": -2, \"y\": 2, \"value\": \"\", \"width\": 3, \"height\": 3}]}"),
                Arguments.of("{\"page\": {\"width\": 12, \"height\": 12}, \"objects\": [{\"type\": \"QR\", \"x\": 2, \"y\": -2, \"value\": \"\", \"width\": 3, \"height\": 3}]}"),
                Arguments.of("{\"page\": {\"width\": 12, \"height\": 12}, \"objects\": [{\"type\": \"QR\", \"x\": 2, \"y\": -2, \"value\": \"\", \"width\": -3, \"height\": 3}]}"),
                Arguments.of("{\"page\": {\"width\": 12, \"height\": 12}, \"objects\": [{\"type\": \"QR\", \"x\": 2, \"y\": 2, \"value\": \"\", \"width\": 3, \"height\": -3}]}"),
                Arguments.of("{\"page\": {\"width\": 12, \"height\": 12}, \"objects\": [{\"type\": \"QR\", \"x\": 2, \"y\": 2, \"value\": null, \"width\": 3, \"height\": 3}]}")
        );
    }

    @ParameterizedTest
    @MethodSource("provideInvalidJson")
    void rejectInvalidJson(String json) {
        Assertions.assertThrows(MismatchedInputException.class, () -> getTemplateReader().readTemplate(new StringReader(json)));
    }

    @ParameterizedTest
    @MethodSource("provideOutOfRangeProperties")
    void rejectOutOfRange(String json) {
        Assertions.assertThrows(ValidationException.class, () -> getTemplateReader().readTemplate(new StringReader(json)));
    }

    @Test
    void objectPlacementOutOfBounds() {
        Assertions.assertThrows(ValidationException.class, () -> getTemplateReader().readTemplate(new StringReader(
                // language=JSON
                "{\n" +
                        "  \"page\": {\n" +
                        "    \"width\": 12,\n" +
                        "    \"height\": 12\n" +
                        "  },\n" +
                        "  \"objects\": [\n" +
                        "    {\n" +
                        "      \"type\": \"QR\",\n" +
                        "      \"x\": 24,\n" +
                        "      \"y\": 24,\n" +
                        "      \"value\": \"\",\n" +
                        "      \"width\": 3,\n" +
                        "      \"height\": 3\n" +
                        "    }\n" +
                        "  ]\n" +
                        "}")));
    }

}