package at.myflexbox.pdf.expression;

import at.myflexbox.pdf.model.DataEntry;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Map;

public class ExpressionEvaluatorTest {

    @Test
    public void expressionWithoutPlaceholder() {
        Assertions.assertNull(ExpressionEvaluator.evaluate(null, null));
        Assertions.assertEquals("", ExpressionEvaluator.evaluate("", null));
        Assertions.assertEquals("some constant string", ExpressionEvaluator.evaluate("some constant string", null));
    }

    @Test
    public void withPlaceholder() {
        Assertions.assertEquals("karl", ExpressionEvaluator.evaluate("{name}", new DataEntry(Map.of("name", "karl"))));
        Assertions.assertEquals("name is karl!", ExpressionEvaluator.evaluate("name is {name}!", new DataEntry(Map.of("name", "karl"))));
        Assertions.assertEquals("name is karl! karl is great!", ExpressionEvaluator.evaluate("name is {name}! {name} is great!", new DataEntry(Map.of("name", "karl"))));
        Assertions.assertEquals("name is karl! karl is great! Some additional params 1", ExpressionEvaluator.evaluate("name is {name}! {name} is great! Some additional params {number}", new DataEntry(Map.of("name", "karl", "number", "1"))));
    }

    @Test
    public void missingParameter() {
        Assertions.assertThrows(RuntimeException.class, () -> ExpressionEvaluator.evaluate("{name}", new DataEntry(Map.of("Name", "karl"))));
    }
}
