# Pdf

MyFlexBox PDF generation evaluation project.

The pdf generation should be implemented in a way, that the least amount of memory is used to generate the PDF files.
To achieve this requirement, the DataReader will not read the whole set of DataEntries into memory, but provide 
an Iterator to provide one DataEntry on demand. 
The PDFRenderer is based on a Producer/Consumer pattern. The main loop will generate RenderTasks for each DataEntry, which renders
this entry to its own PDF file. A ExecutorService is not used here, to avoid generating the whole Tasks upfront. 
So the setup will block when the WorkQueue reaches its capacity limits. The available compute resources limit the data fetch and
production automatically and therefore limit memory pressure because only the current work items are in memory.

Basic Architecture
```mermaid
sequenceDiagram
    main->TemplateReader: read template + basic validation
    TemplateReader->main: template object
    main->DataReader: read data + basic validation
    DataReader->main: data object
    main->PDFRenderer: apply data to template and render pdf
    main->FileWriter: write PDF
```

PDF rendering
```mermaid
sequenceDiagram
    loop for every object in template
        PDFRenderer->RenderFunction: render specific object type
        RenderFunction->ExpressionEvaluator: extract final values
        RenderFunction->Validator: pre render validation
        RenderFunction->render: render item
    end
```

changes when batch processing is needed
```mermaid
sequenceDiagram
    main->TemplateReader: read template + basic validation
    TemplateReader->main: template object
    loop for every data entry
        main->DataReader: read data + basic validation
        DataReader->main: data entry
        main->PDFRenderer: apply data to template and render pdf and write file
    end
```

### PDF
PDF coordinate system is based on left bottom corner (y == 0 is bottom). So to meet the requirements, one has to calculate
the difference between the height and the y coordinate. 

### run
```bash
java -jar target\pdf-0.0.1-SNAPSHOT.jar --template=template.json --data=data.json --output=C:\temp\pdfs
```
